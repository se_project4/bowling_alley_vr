# Bowling Arena
Welcome to a VR based bowling alley game created using unity Engine

## Collaboration Team 18
-   Siddharth Gupta (2021201082) 
-   Josh Joy (2021204009)  
-   Bhupendra Sharma (2021201020)  
-   Aditya Mahajan (2020202017) 

## Test and Deploy

- Directly deploy apk bowling_alley_v4.apk in oculus vr quest
- Run and enjoy the game

***

## Visuals
![alt text](/screenshots/Screenshot%20(162).png)
![alt text](/screenshots/Screenshot%20(169).png)

