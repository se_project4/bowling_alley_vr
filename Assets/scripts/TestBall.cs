using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

using System;
using System.Globalization;
using System.IO;
using UnityEngine.UI;



public class TestBall : MonoBehaviour
{
    public Rigidbody rb;
    public TextMeshProUGUI Score;
    public TextMeshProUGUI Info;
    public TextMeshProUGUI TotalScore;
    public TextMeshProUGUI CurrentThrow;
    public TextMeshProUGUI HighScore;
    private int c_score = 0;
    private int ball_chance = 0;
    private int current_throw = 1;
    // private int current_frame = 1;

    private bool gameFinished = false;

    private int[] frame_score = new int[10];
    private int[] ball_on_alley = new int[10];
    private int TotalScore_count=0;
    private int strike=0;

    public Vector3 ball_init;
    public List<Vector3> pins_pos;
    public List<Quaternion> pins_rotation;
    public List<GameObject> down_balls;
    // Start is called before the first frame update

    public Text[] scores;
    string filepath;


    private List<int> ReadFile()
    {
        using (StreamReader reader = File.OpenText(filepath))
        {

            List<int> numbers = new List<int>();

            while (true)
            {
                string FindMax = reader.ReadLine();
                if (FindMax == null)
                {
                    break;
                }
                int test;
                if (Int32.TryParse(FindMax, out test))
                {
                    numbers.Add(test);
                }
            }
            // Console.WriteLine("the highest number is {0}", numbers.Max());
            int len = numbers.Count;
            List<int> num = new List<int>();
            string s="High Score: ";
            if (len < 10)
            {
                for (int i = 0; i < len; i++)
                {
                    num.Add(numbers[i]);
                   
                }
            }
            else
            {
                for (int i = len - 10; i < len; i++)
                {
                    num.Add(numbers[i]);
                }
            }

            
            num.Sort((a, b) => b.CompareTo(a));

            for (int i=0;i<len;i++)
            {
                s = s + num[i].ToString() + " ";
            }

            HighScore.SetText(s);
            return num;
        }
    }


    private void CreateFile()
    {
        if (!File.Exists(filepath))
        {
            File.Create(filepath).Close();
            WriteFile();
        }
        
    }

    private void WriteFile()
    {
        using (StreamWriter writer = File.AppendText(filepath))
        {
            writer.WriteLine(TotalScore_count.ToString());
        }
    }

    public void Save()
    {
        CreateFile();
        WriteFile();
    }

    


    void Start()
    {
        filepath = Application.persistentDataPath + "/Scores.txt";
        Debug.Log(filepath);
        CreateFile();
        List<int> numbers = new List<int>();
        numbers = ReadFile();

        for (int i = 0; i < numbers.Count; i++)
        {
            Debug.Log("Hello there");
            Debug.Log(numbers[i]);
        }

        ball_init = gameObject.transform.position;
        for(int i=0;i<10;i++) 
        {
            int j=i+1;
            pins_pos.Add( GameObject.FindGameObjectsWithTag("Pin"+j.ToString())[0].transform.position );
            pins_rotation.Add( GameObject.FindGameObjectsWithTag("Pin"+j.ToString())[0].transform.rotation );
            ball_on_alley[i] = 1;
            frame_score[i] = -1;

            if(i==9)
            {
                int cou = 0;
                foreach (GameObject g in GameObject.FindGameObjectsWithTag("Pin" + j.ToString()))
                    cou++;

                Debug.Log(" Cou" +cou.ToString());
            }

        }

        Debug.Log("Task Completed");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        rb.AddForce(Vector3.left * 2000);
    }

    bool approx(int i,Quaternion quatA, Quaternion quatB, float acceptableRange)
    {
        Debug.Log(i);
        Debug.Log(1 - Mathf.Abs(Quaternion.Dot(quatA, quatB)));
        return 1 - Mathf.Abs(Quaternion.Dot(quatA, quatB)) < acceptableRange;
    }

    void ResetBalls()
    {
        c_score = 0;
        ball_chance = 0;
        strike = 0;
        Info.SetText("");
        gameObject.transform.position = ball_init;
        GameObject.FindGameObjectsWithTag("TestBall")[0].GetComponent<Rigidbody>().velocity = Vector3.zero;
        if(current_throw == 10)
        {
            Info.SetText("Game Finished");
            gameFinished = true;
            Save();
            // create Save file if not exist



            return;
        }

        for(int i=1;i<=10;i++)
        {
            GameObject.FindGameObjectsWithTag("Pin"+i.ToString())[0].transform.position = pins_pos[i-1];
            GameObject.FindGameObjectsWithTag("Pin"+i.ToString())[0].transform.rotation = pins_rotation[i-1];
            GameObject.FindGameObjectsWithTag("Pin"+i.ToString())[0].GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        
        current_throw++;
        CurrentThrow.SetText("CurrentThrow:"+current_throw);
    }

    private void countPins()
    {
        down_balls.Clear();
        for(int i=1;i<=10;i++)
        {
            if(!approx(i, GameObject.FindGameObjectsWithTag("Pin"+i.ToString())[0].transform.rotation, pins_rotation[i-1], 0.1f))
            {
                c_score++;
                down_balls.Add( GameObject.FindGameObjectsWithTag("Pin"+i.ToString())[0] );
                ball_on_alley[i-1] = 0;
            }
        }
        TotalScore_count+=c_score;
        if(current_throw == 10)
        Score.SetText(Score.text+"|"+c_score.ToString()+"|");
        else
        Score.SetText(Score.text+"|"+c_score.ToString());
        TotalScore.SetText("Score:"+TotalScore_count.ToString());
        Info.SetText("Info:Loading Next Throw");
        Invoke("ResetBalls",3f);

    }
    private bool ballStrike(Collision c)
    {
        if(
            c.gameObject.tag == "Pin1" || 
            c.gameObject.tag == "Pin2" ||
            c.gameObject.tag == "Pin3" ||
            c.gameObject.tag == "Pin4" ||
            c.gameObject.tag == "Pin5" ||
            c.gameObject.tag == "Pin6" ||
            c.gameObject.tag == "Pin7" ||
            c.gameObject.tag == "Pin8" ||
            c.gameObject.tag == "Pin9" ||
            c.gameObject.tag == "Pin10" 
          )
          return true;
          return false;
    }
    private void OnCollisionEnter(Collision c)
    {
        ball_chance++;
        if(gameFinished)
        {
            Info.SetText("Info:Game has been finished,Please Restart to play again!");
            return;
        }
        if(ballStrike(c) && strike == 0)
        {
            strike = 1;
            Invoke("countPins",15f);
        }
    }
}
